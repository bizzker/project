$(document).ready(function () {
    $('#comment-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this),
            data = form.serializeArray(),
            dataObj = {};
        for (var i=0; i<data.length; i++) {
            dataObj[data[i].name] = data[i].value;
        }

        $.ajax({
            url: form.attr('action'),
            data: data,
            method: 'POST',
            success: function (response) {
                console.log(response);
                var cardBlock = $('#empty-card').clone(true).show().prop('id', ''),
                    commentsBox = $('#comments-box');
                cardBlock.prependTo(commentsBox);
                cardBlock.find('.title').text(dataObj.name);
                cardBlock.find('.text').text(dataObj.text_comment);
            },
            error: function (response) {
                alert('error');
            }
        });
    });
});