$(document).ready(function () {
    var element = $('.js-image-change');
    function setAlternateSrc(el) {
        var altSrc = el.attr('data-alt-src');
        var originSrc = el.attr('src');
        el.attr('data-alt-src', originSrc);
        el.attr('src', altSrc);
    }
    function restoreSrc(el) {
        var originSrc = el.attr('data-alt-src');
        var altSrc = el.attr('src');
        el.attr('data-alt-src', altSrc);
        el.attr('src', originSrc);
    }
    element.mouseover(function () {
        setAlternateSrc($(this));
    });
    element.mouseout(function () {
        restoreSrc($(this));
    });
});