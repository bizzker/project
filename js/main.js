/******************** Form Validation ***********************/

function showError(container, errorMessage) {
    container.className = 'error';
    var msgElem = document.createElement('span');
    msgElem.className = "error-message";
    msgElem.innerHTML = errorMessage;
    container.appendChild(msgElem);
}

function resetError(container) {
    container.className = '';
    if (container.lastChild.className = "error-message") {
        container.removeChild(container.lastChild);
    }
}

function validate(form) {
    var elems = form.elements;
    resetError(elems.user.parentNode);
    if (!elems.user.value) {
        showError(elems.user.parentNode, '  Вкажіть свій username');
    }
    resetError(elems.email.parentNode);
    if (!elems.email.value) {
        showError(elems.email.parentNode, ' Вкажіть email');
    }
    resetError(elems.message.parentNode);
    if (!elems.message.value) {
        showError(elems.message.parentNode, 'Введіть текст у вищезазначене поле');
    }
}

/************** Show-Hide ********************/

$(document).ready(function () {
    $('.js-hide-block').click(function () {
        var box = $(this).parents('div').find('.toggle');
        if (box.is(':visible')) {
            box.hide('slow');
        }
        else {
            box.show('slow');
            console.log(box);
        }
    });
});

/****************** New window ***********************/

$(document).ready(function () {
    $('.link-button').click(function (event) {
        var windowSizeArray = ["width=600,height=1000", "width=600,height=1000"];
        var url = $(this).attr("href");
        var windowName = $(this).attr("name");
        var windowSize = windowSizeArray[$(this).attr("rel")];
        window.open(url, windowName, windowSize);
        event.preventDefault();
    });
});