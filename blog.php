<?php
$mysqli = new mysqli("localhost", "root", "", "comments");
$result_set = $mysqli->query("SELECT * FROM `comments` ORDER BY `created_at` DESC");
$data = [];
while ($row = $result_set->fetch_array()) {
    $data[] = $row;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./style/style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>IziKuxnya</title>
</head>
<body id="body-page">
<header>
    <div class="top-bg">
        <div class="login">
            <button class="js-hide-block">Login</button>
        </div>
        <div class="main">
            <div class="top-raw">
                <div class="top-text">
                    <img src="./style/images/logo.png" class="logo">
                    <h1>online cooking recipes</h1>
                </div>
                <div class="modal toggle">
                    <form class="form-content" action="#">
                        <div class="imgcontainer">
                            <img src="./style/images/logo.png" alt="avatar" class="avatar">
                        </div>
                        <div class="form-container">
                            <label><b>Username</b></label>
                            <input type="text" placeholder="Enter Username" name="uname" required/>
                            <label><b>Password</b></label>
                            <input type="password" placeholder="Enter Password" name="psw" required/>
                            <button type="submit" class="q">Login</button>
                            <label for="remb">Remember me</label>
                            <input type="checkbox" id="remb" name="check" class="check" checked/>
                            <button type="submit">Register</button>
                        </div>
                        <div class="form-container">
                            <button type="button" class="js-hide-block">Cancel</button>
                            <span>Forgot <a href="#">password?</a> </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-row">
        <div class="main">
            <nav>
                <div class="menu">
                    <a class="button" href="index.html">Головна</a>
                    <a class="button" href="about.html">Про нас</a>
                    <a class="button" href="recipes.html">Нові рецепти</a>
                    <a class="button" href="blog.php">Блог</a>
                    <a class="button" href="contacts.html">Контакти</a>
                </div>
            </nav>
        </div>
    </div>
</header>
<main class="main">
    <article class="inner-box">
        <article class="content">
            <h2 class="title"><span>Lorem</span> Ipsum</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ullamcorper gravida
                sapien, eget hendrerit diam finibus at. Pellentesque eleifend, tellus sed eleifend
                posuere, nulla arcu euismod risus, sed viverra quam eros ut turpis. Nam nec dapibus
                ipsum, ut laoreet nunc. Etiam sit amet ligula commodo, porttitor justo et, volutpat
                tellus. Nam enim nisi, rhoncus vitae enim in, dapibus efficitur sapien. Nunc vulputate
                est id massa varius, vel elementum libero consequat. Nulla commodo magna vitae enim
                vestibulum, id condimentum purus consequat. Suspendisse et facilisis felis.
                Nunc dapibus, massa sed volutpat accumsan, sem ipsum porttitor urna, id consequat
                urna ante ut turpis. Duis ipsum tellus, mattis pharetra bibendum a, convallis eu leo.</p>
        </article>
        <article class="content">
            <img class="resipes-img js-image-change" src="./style/images/peper.png"
                 data-alt-src="./style/images/peper2.png" alt="Спеції">
            <h3 class="title">Lorem Ipsum</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim lacinia
                dui id auctor. Vestibulum vehicula pharetra justo vitae laoreet.</p>
            <a class="link-button" rel="0" href="#">Read More</a>
        </article>
        <article class="content">
            <h2 class="title"><span>Чому</span> ви <span>маєте обрати саме</span> нас <span>?</span></h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim lacinia
                dui id auctor. Vestibulum vehicula pharetra justo vitae laoreet. Suspendisse potenti.
                Ut ut tincidunt urna, eget tristique enim. Aenean interdum massa et nisi suscipit,
                at suscipit enim rhoncus. Aliquam et lobortis lectus. Phasellus ante libero,
                ultrices in massa eget, mollis rutrum tellus. Nullam non pretium dui, sed mollis orci.
                Suspendisse malesuada erat arcu, eget malesuada magna rhoncus nec.
                Ut et sapien sed eros egestas condimentum a ac Phasellus ante libero,
                ultrices in massa eget, mollis rutrum tellus.eros. Morbi vel vestibulum lectus,
                ut varius libero. Mauris placerat nisi et ligula facilisis sollicitudin.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non pretium dui, sed mollis orci.
                Suspendisse malesuada erat arcu, eget malesuada magna rhoncus nec. Ut ut tincidunt urna, eget tristique
                enim. Aenean interdum massa et nisi suscipit,
                at suscipit enim rhoncus. Aliquam et lobortis lectus. Phasellus ante libero,
                ultrices in massa eget, mollis rutrum tellus. Nullam non pretium dui, sed mollis orci.</p>
        </article>
        <img class="candys-img js-image-change" src="./style/images/fr.png" data-alt-src="./style/images/fr2.png"
             alt="Солодощі">
    </article>
    <div class="right-box">
        <a class="quote-button js-hide-block"><p>REVIEWS</p></a>
        <div id="show-hide" class="quotes right-bar toggle">
            <div class="comments-form">
                <form name="comment" id="comment-form" action="./storeComments.php" method="post">
                    <label for="name" class="title">Ім'я : </label><br>
                    <input type="text" name="name" id="name" required><br>
                    <label for="comment" class="title">Відгук :</label><br>
                    <textarea id="comment" name="text_comment" cols="40" rows="5" required></textarea><br>
                    <button id="button" type="submit">Відправити</button>
                </form>
            </div>
            <div id="comments-box">
                <div id="empty-card" class="card" style="display: none">
                    <img src="./style/images/web-user.png" class="card-avatar" alt="Avatar">
                    <h4 class="title"><b></b></h4>
                    <p class="text"></p>
                </div>
                <?php foreach ($data as $value) { ?>
                    <div class="card">
                        <img src="./style/images/web-user.png" class="card-avatar" alt="Avatar">
                        <h4 class="title"><b><?= $value['name'] ?></b></h4>
                        <p class="text"><?= $value['text_comment'] ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<footer>
    <div class="btm-bg">
        <div class="main">
            <div class="footer-row">
                <div class="footer-text">
                    Copyright <span class="title">IziKuxnya</span> &copy; 2018
                    <strong>All rights reserved</strong>
                </div>
                <ul class="list-services">
                    <li>Link to Us:</li>
                    <li class="item"><a title="facebook" target="_blank" href="https://www.facebook.com"></a></li>
                    <li class="item-1"><a title="twitter" target="_blank" href="https://twitter.com"></a></li>
                    <li class="item-2"><a title="linkedin" target="_blank" href="https://www.linkedin.com/"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="./js/main.js" type="text/javascript"></script>
<script src="./js/imgchange.js" type="text/javascript"></script>
<script src="./js/blog.js" type="text/javascript"></script>
</body>
</html>

