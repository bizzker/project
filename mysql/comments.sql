-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 25 2018 г., 13:14
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `comments`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `name` varchar(255) NOT NULL,
  `text_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`name`, `text_comment`) VALUES
('Галина', 'Добрый день!\r\nКаждый раз, заходя на ваш сайт, восхищаюсь тем, насколько он удобный!\r\nОгромное спасибо программистам, которые его придумали, персоналу, который его обслуживает и, конечно же, вашему руководству за грамотный подход!'),
('Микола Володимирович', 'Хочу Вам подякувати за чудово розроблений сайт. Дуже зручно користуватися. Багато корисної і наглядної інформації.'),
('Ангеліна', 'Чоловікові тортик дуже сподобався! Свіженький, смачний і дуже красиво оформлений! Рекомендую всім!)'),
('Олександр Місько', 'Чудовий сайт !!'),
('Олег Винник', 'Спасибі за послугу. Сайт зручний в користуванні, все зрозуміло й доступно...'),
('Оленка', 'Дякуємо від всіх гостей за смачненьке частування. Торт для нашої маленької іменинниці получився саме таким як хотіли. Особливо порадувало оформлення — воно точнісінько як ми і хотіли.\r\nВи однозначно отримали постійних клієнтів'),
('Ярослав Гончарук', 'Добрий день! Мене звати Ярослав, я живу в Кракові, Ваше прекрасне подання, як готувати їжу, допровадило мене, що я почав сам час від часу щось робити на кухні, і завдяки Вашій праці мені все вдавалося. Борщ наш, такий як вдома, і вареники і плов і хачапурі, а до цього я був такий далекий від кухні .Дуже Вам дякую!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
